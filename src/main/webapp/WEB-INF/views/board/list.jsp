<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sample 게시판</title>
    <meta name="description" content="게시판 - 목록">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sample.css" />
    <script src="${pageContext.request.contextPath}/resources/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function() {
            // 등록폼 페이지로 이동
            $("#registerBtn").click(function() {
                // $(location).attr("href", "registerForm");
            });
        });
    </script>
</head>
<body>

<div class="content">
    <h1>게시판 목록</h1>
    <form id="searchForm" name="searchForm" method="post">
        <input type="hidden" id="id" name="id" />
    </form>
    <table class="list">
        <colgroup>
            <col width="10%" />
            <col width="60%" />
            <col width="10%" />
            <col width="10%" />
            <col width="10%" />
        </colgroup>
        <thead>
        <tr>
            <th>No</th>
            <th>제목</th>
            <th>조회수</th>
            <th>등록자</th>
            <th>등록일</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${not empty boards}">
                <c:forEach var="board" items="${boards}" varStatus="i">
                    <tr>
                        <td>${fn:length(boards) - i.index}</td>
                        <td class="align-left">${board.title}</td>
                        <td>${board.viewCnt}</td>
                        <td>${board.author}</td>
                        <td><fmt:formatDate value="${board.regDate}" pattern="yyyy-MM-dd" /></td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td colspan="5">검색 조건에 해당하는 글이 없습니다.</td>
                </tr>
            </c:otherwise>
        </c:choose>
        </tbody>
    </table>
    <div class="btn-wrap">
        <input type="button" id="registerBtn" value="등록" class="def_btn" />
    </div>
</div>

</body>
</html>