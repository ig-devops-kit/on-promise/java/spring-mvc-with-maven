<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html class="no-js" lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Spring MVC Application</title>
    <meta name="description" content="Hello World Application">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="<c:url value="/resources/css/sample.css"/>" rel="stylesheet">
</head>
<body>
  <h1 style="color: green;">Sample Board Web Application #2</h1>
</body>
</html>
