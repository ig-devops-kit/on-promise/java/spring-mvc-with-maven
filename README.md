# Spring MVC with Maven
Spring MVC - Web Demo Maven CI/CD 파이프라인 예제
#### 환경
- Java version 1.8
- Spring version 3.2.11
- Servlet version 3.1.0
- SLF4J version 1.7.7
- Logback version 1.1.2
- Jackson version 2.4.3


#### 파이프라인 정보
- **Maven Basic** 
  <!-- [maven:build, maven:test] -->
- **Spring Boot Maven with SSH JAR** 
  <!-- [maven:build, maven:test, security:semgrep-sast, security:code-quality, security:secret_detection, deploy:jar] -->
- **Spring Boot Maven with Docker Compose**
  <!-- [maven:build, maven:test, security:semgrep-sast, security:code-quality, security:secret_detection, security:container_scanning, docker:kaniko, deploy:docker-compose] -->